drop database ProjektDorMarJan;

create database ProjektDorMarJan;

USE ProjektDorMarJan;

create table QuizUser
(
	id integer auto_increment NOT NULL,
    Username varchar(255) NOT NULL,
    Email varchar(50) NOT NULL,
    Password longtext NOT NULL,
    constraint pk_questions primary key(id)
);

create table Other
(
id integer auto_increment NOT NULL,
Question varchar(255) NOT NULL,
Answer1 varchar(255) NOT NULL,
Answer2 varchar(255) NOT NULL,
Answer3 varchar(255) NOT NULL,
Answer4 varchar(255) NOT NULL,
Correct varchar(255) NOT NULL,
UserID integer NOT NULL,
Valid boolean NOT NULL,
constraint pk_quiz primary key(id),
constraint fk_key foreign key(UserID)references QuizUser(id)
);

create table Geography
(
id integer auto_increment NOT NULL,
Question varchar(255) NOT NULL,
Answer1 varchar(255) NOT NULL,
Answer2 varchar(255) NOT NULL,
Answer3 varchar(255) NOT NULL,
Answer4 varchar(255) NOT NULL,
Correct varchar(255) NOT NULL,
UserID integer NOT NULL,
Valid boolean NOT NULL,
constraint pk_quiz primary key(id),
constraint Geo_key foreign key(UserID)references QuizUser(id)
);

create table History
(
id integer auto_increment NOT NULL,
Question varchar(255) NOT NULL,
Answer1 varchar(255) NOT NULL,
Answer2 varchar(255) NOT NULL,
Answer3 varchar(255) NOT NULL,
Answer4 varchar(255) NOT NULL,
Correct varchar(255) NOT NULL,
UserID integer NOT NULL,
Valid boolean NOT NULL,
constraint pk_quiz primary key(id),
constraint Hi_key foreign key(UserID)references QuizUser(id)
);

create table Literature
(
id integer auto_increment NOT NULL,
Question varchar(255) NOT NULL,
Answer1 varchar(255) NOT NULL,
Answer2 varchar(255) NOT NULL,
Answer3 varchar(255) NOT NULL,
Answer4 varchar(255) NOT NULL,
Correct varchar(255) NOT NULL,
UserID integer NOT NULL,
Valid boolean NOT NULL,
constraint pk_quiz primary key(id),
constraint Li_key foreign key(UserID)references QuizUser(id)
);

create table Score
(
id integer auto_increment NOT NULL,
ScoreOther integer,
ScoreGeography integer,
ScoreHistory integer,
ScoreLiterature integer,
UserID integer NOT NULL,
constraint pk_Score primary key(id),
constraint scFk_key foreign key(UserID)references QuizUser(id)
);

select * from QuizUser;
select * from Geography;
select * from History;
select * from Literature;
select * from Other;
